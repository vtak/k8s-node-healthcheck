package main

import (
	"fmt"
	"io"
	"net/http"
)

func handle(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "success")
}

func main() {
	portNumber := "8080"
	http.HandleFunc("/", handle)
	// http.HandleFunc("/healthz", handle)
	fmt.Println("Server listening on port ", portNumber)
	http.ListenAndServe(":"+portNumber, nil)
}
