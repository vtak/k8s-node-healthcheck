# k8s-node-healthcheck

Simple health check server to run on each k8s node to perform health-check

### gitlab-docker-registry-secret
```
kubectl create secret docker-registry personal-image-pull-credential-vtak --docker-server=registry.gitlab.com --docker-username=k8s --docker-password=<token>

kubectl get secret personal-image-pull-credential-vtak -o yaml
```
