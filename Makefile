BINDIR := $(CURDIR)/bin
DOCKER_REGISTRY := registry.gitlab.com
DOCKER_REPOSITORY_NAME := vtak/k8s-node-healthcheck
DOCKER_REPOSITORY := $(DOCKER_REGISTRY)/$(DOCKER_REPOSITORY_NAME)
HELM_CHART_NAME := k8s-node-healthcheck
HELM_CHART_DIR := $(CURDIR)/helm
HELM_VALUES_FILE := $(HELM_CHART_DIR)/values.yaml
HELM_NAMESPACE := default
SCRIPT_DIR := $(CURDIR)/scripts

.PHONY: all build clean

all: k8s-node-healthcheck

build:
	$Q GOBIN=$(BINDIR) go install -buildmode exe

clean:
	$Q GOBIN=$(BINDIR) go clean -i -modcache -x

k8s-node-healthcheck: build
	$Q cp -f $(BINDIR)/k8s-node-healthcheck .

helm-template:
	$Q helm template --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) $(HELM_CHART_NAME) $(HELM_CHART_DIR)

helm-diff:
	$Q helm diff upgrade --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) --install --context 0 $(HELM_CHART_NAME) $(HELM_CHART_DIR)

helm-upgrade: helm-diff
	$Q $(SCRIPT_DIR)/prompt_confirm.sh
	$Q helm upgrade --namespace $(HELM_NAMESPACE) -f $(HELM_VALUES_FILE) --install $(HELM_CHART_NAME) $(HELM_CHART_DIR)

docker-login:
	$Q docker login $(DOCKER_REGISTRY)

docker-build:
	$Q docker build -t ${DOCKER_REPOSITORY} .

docker-push: docker-build docker-login
	$Q docker push ${DOCKER_REPOSITORY}
