prompt_confirm() {
  read -r -n 1 -p "${1:-Continue?} [y/n]: " REPLY
  if [ "$REPLY" == "y" ]
  then
    echo "\nContinuing upgrade as per input"
  else
    echo "\nAborting upgrade as per input"
    exit 1
  fi
}

prompt_confirm "Continue?" || exit 0
